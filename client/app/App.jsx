// import React from 'react';
//
// class App extends React.Component {
//      constructor () {
//           super();
//           this.state = {
//                data:
//                [
//                     {
//                          "id":1,
//                          "name":"Foo",
//                          "age":"20"
//                     },
//
//                     {
//                          "id":2,
//                          "name":"Bar",
//                          "age":"30"
//                     },
//
//                     {
//                          "id":3,
//                          "name":"Baz",
//                          "age":"40"
//                     }
//                ]
//           }
//      }
//      render() {
//           return (
//                <div>
//                     <Header/>
//                     <table>
//                          <tbody>
//                               {this.state.data.map((person, i) => <TableRow key = {i} data = {person} />)}
//                          </tbody>
//                     </table>
//                     <Footer/>
//                </div>
//           );
//      }
// }
//
// class Header extends React.Component {
//      render() {
//           return (
//                <h1>Header</h1>
//           );
//      }
// }
//
// class Footer extends React.Component {
//      render() {
//           return (
//                // var i = 1;
//                // var style = {
//                //      color : 'red'
//                // }
//                // return (
//                //      <div>
//                //           <p data-myattribute = "somevalue">This is the content!!!</p>
//                //           <h6>{1+1}</h6>
//                //           <h6 style = {style}>{i = 1 ? 1:2}</h6>
//                //      </div>
//                // );
//                <h1>Footer</h1>
//           )
//      }
// }
//
// class TableRow extends React.Component {
//    render() {
//       return (
//          <tr>
//             <td>{this.props.data.id}</td>
//             <td>{this.props.data.name}</td>
//             <td>{this.props.data.age}</td>
//          </tr>
//       );
//    }
// }
//
//
// export default App;

// import React from 'react';
//
// class App extends React.Component {
//    render() {
//       return (
//          <div>
//             <h3>Array: {this.props.propArray}</h3>
//             <h3>Bool: {this.props.propBool ? "True..." : "False..."}</h3>
//             <h3>Func: {this.props.propFunc(3)}</h3>
//             <h3>Number: {this.props.propNumber}</h3>
//             <h3>String: {this.props.propString}</h3>
//             <h3>Object: {this.props.propObject.objectName1}</h3>
//             <h3>Object: {this.props.propObject.objectName2}</h3>
//           </div>
//       );
//    }
// }
//
// App.propTypes = {
//    propArray: React.PropTypes.array.isRequired,
//    propBool: React.PropTypes.bool.isRequired,
//    propFunc: React.PropTypes.func,
//    propNumber: React.PropTypes.number,
//    propString: React.PropTypes.string,
//    propObject: React.PropTypes.object
// }
//
// App.defaultProps = {
//    propArray: [1,2,3,4,5],
//    propBool: true,
//    propFunc: function(e){return e},
//    propNumber: 1,
//    propString: "String value...",
//
//    propObject: {
//       objectName1:"objectValue1",
//       objectName2: "objectValue2",
//       objectName3: "objectValue3"
//    }
// }
//
// export default App;

// import React from 'react';
//
// class App extends React.Component {
//    constructor() {
//       super();
//
//       this.state = {
//          data: []
//       }
//
//       this.setStateHandler = this.setStateHandler.bind(this);
//    };
//
//    setStateHandler() {
//       var item = "setState..."
//       var myArray = this.state.data;
//       myArray.push(item)
//       this.setState({data: myArray})
//    };
//
//    render() {
//       return (
//          <div>
//             <button onClick = {this.setStateHandler}>SET STATE</button>
//             <h4>State Array: {this.state.data}</h4>
//          </div>
//       );
//    }
// }
//
// export default App;

// import React from 'react';
//
// class App extends React.Component {
//    constructor() {
//       super();
//       this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
//    };
//
//    forceUpdateHandler() {
//       this.forceUpdate();
//    };
//
//    render() {
//       return (
//          <div>
//             <button onClick = {this.forceUpdateHandler}>FORCE UPDATE</button>
//             <h4>Random number: {Math.random()}</h4>
//          </div>
//       );
//    }
// }
//
// export default App;

// import React from 'react';
// import ReactDOM from 'react-dom';
//
// class App extends React.Component {
//    constructor() {
//       super();
//       this.findDomNodeHandler = this.findDomNodeHandler.bind(this);
//    };
//
//    findDomNodeHandler() {
//       var myDiv = document.getElementById('myDiv');
//       ReactDOM.findDOMNode(myDiv).style.color = 'green';
//    }
//
//    render() {
//       return (
//          <div>
//             <button onClick = {this.findDomNodeHandler}>FIND DOME NODE</button>
//             <div id = "myDiv">NODE</div>
//          </div>
//       );
//    }
// }
//
// export default App;

//
// import React from 'react';
//
// class App extends React.Component {
//
//    constructor(props) {
//       super(props);
//
//       this.state = {
//          data: 0
//       }
//
//       this.setNewNumber = this.setNewNumber.bind(this)
//    };
//
//    setNewNumber() {
//       this.setState({data: this.state.data + 1})
//    }
//
//    render() {
//       return (
//          <div>
//             <button onClick = {this.setNewNumber}>INCREMENT</button>
//             <Content myNumber = {this.state.data}></Content>
//          </div>
//       );
//    }
// }
//
// class Content extends React.Component {
//
//    componentWillMount() {
//       console.log('Component WILL MOUNT!')
//    }
//
//    componentDidMount() {
//       console.log('Component DID MOUNT!')
//    }
//
//    componentWillReceiveProps(newProps) {
//       console.log('Component WILL RECIEVE PROPS!')
//    }
//
//    shouldComponentUpdate(newProps, newState) {
//       return true;
//    }
//
//    componentWillUpdate(nextProps, nextState) {
//       console.log('Component WILL UPDATE!');
//    }
//
//    componentDidUpdate(prevProps, prevState) {
//       console.log('Component DID UPDATE!')
//    }
//
//    componentWillUnmount() {
//       console.log('Component WILL UNMOUNT!')
//    }
//
//    render() {
//       return (
//          <div>
//             <h3>{this.props.myNumber}</h3>
//          </div>
//       );
//    }
// }
//
// export default App;
//
// import React from 'react';
//
// class App extends React.Component {
//
//    constructor(props) {
//       super(props);
//
//       this.state = {
//          data: 'Initial data...'
//       }
//
//       this.updateState = this.updateState.bind(this);
//
//    };
//
//    updateState(e) {
//       this.setState({data: e.target.value});
//    }
//
//    render() {
//       return (
//          <div>
//             <input type = "text" value = {this.state.data}
//                onChange = {this.updateState} />
//             <h4>{this.state.data}</h4>
//          </div>
//       );
//    }
// }
//
// export default App;
// import React from 'react';
//
// class App extends React.Component {
//
//    constructor(props) {
//       super(props);
//
//       this.state = {
//          data: 'Initial data...'
//       }
//
//       this.updateState = this.updateState.bind(this);
//    };
//
//    updateState() {
//       this.setState({data: 'Data updated from the child component...'})
//    }
//
//    render() {
//       return (
//          <div>
//             <Content myDataProp = {this.state.data}
//                updateStateProp = {this.updateState}></Content>
//          </div>
//       );
//    }
// }
//
// class Content extends React.Component {
//
//    render() {
//       return (
//          <div>
//             <button onClick = {this.props.updateStateProp}>CLICK</button>
//             <h3>{this.props.myDataProp}</h3>
//          </div>
//       );
//    }
// }
//
// export default App;
//
// import React from 'react';
// import ReactDOM from 'react-dom';
// import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router'
//
// class App extends React.Component {
//    render() {
//       return (
//          <div>
//             <ul>
//                <li>Home</Link>
//                <li>About</Link>
//                <li>Contact</Link>
//             </ul>
//
//            {this.props.children}
//          </div>
//       )
//    }
// }
//
// export default App;
//
// class Home extends React.Component {
//    render() {
//       return (
//          <div>
//             <h1>Home...</h1>
//          </div>
//       )
//    }
// }
//
// export default Home;
//
// class About extends React.Component {
//    render() {
//       return (
//          <div>
//             <h1>About...</h1>
//          </div>
//       )
//    }
// }
//
// export default About;
//
// class Contact extends React.Component {
//    render() {
//       return (
//          <div>
//             <h1>Contact...</h1>
//          </div>
//       )
//    }
// }
//
// export default Contact;
import React from 'react';
import { Link } from 'react-router';
class App extends React.Component {
     render() {
          return (
               <div>
                    <ul>
                         <Link to="/home">Home</Link>  ||
                         <Link to="/about">About</Link>  ||
                         <Link to="/contact">Contact</Link>
                    </ul>
               </div>
          )
     }
}

export default App;
