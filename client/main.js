// import React from 'react';
// import ReactDOM from 'react-dom';
// import App from './App.jsx';
//
// ReactDOM.render(<App />, document.getElementById('app'));

// setTimeout(() => {
//    ReactDOM.unmountComponentAtNode(document.getElementById('app'));}, 10000);

// import React from 'react';
// import ReactDOM from 'react-dom';
// import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';
// import App from './App.jsx';

import React,{ Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';
import App from './app/App.jsx';
import Home from './app/Home.jsx';
import About from './app/About.jsx';
import Contact from './app/Contact.jsx';

ReactDOM.render((

   <Router history = {browserHistory}>
      <Route path = "/" component = {App}>
         <IndexRoute component = {Home} />
         <Route path = "/home" component = {Home} />
         <Route path = "/about" component = {About} />
         <Route path = "/contact" component = {Contact} />
      </Route>
   </Router>

), document.getElementById('app'))
