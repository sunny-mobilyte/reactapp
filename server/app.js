const express = require('express')
const path = require('path')
const port = process.env.PORT || 3222
const app = express()
global.APP_PATH = path.resolve(__dirname);

app.set('views', path.join(__dirname, '/client/public'));
// serve static assets normally
app.use(express.static(__dirname + '/client/public'));

var fs = require('fs');
app.engine('html', function (path, opt, fn) {
     fs.readFile(path, 'utf-8', function(err, str) {
          if(err) return str;
          return fn (null, str);
     })
});


// Handles all routes so you do not get a not found error
app.get('*', function (request, response){
    response.render(APP_PATH+'/client/public/index.html')
});

app.listen(port);
console.log("server started on port " + port);
